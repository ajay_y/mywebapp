package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {
	@Autowired
	UserService userService;
	
	@GetMapping("/")
	void getUsers() {
	System.out.println("called");
	}
	@GetMapping("/{id}")
	void getUser(@PathVariable Integer id) {
	System.out.println("called...." + id);
	}
	@PostMapping("/name")
	private String saveUser(@RequestBody User user) {
	userService.save(user);
	System.out.println("user name -- " + user.getName());
	return "post called";
	}
	@PostMapping("/age")
	private String saveAge(@RequestBody User user) {
	userService.save(user);
	System.out.println("user age -- " + user.getAge());
	return "age called";
	}
	@PutMapping
	private String putcall() {
		System.out.println("put");
		return "put called";

	}
}
